require('dotenv').config();
// eslint-disable-next-line import/first
import { ApolloServer } from 'apollo-server';
// eslint-disable-next-line import/first
import schema from './schema';
// import { typeDefs, resolvers } from './schema';

const server = new ApolloServer({
  schema,
});

const {PORT} = process.env;

server.listen(PORT).then(() => {
  console.log(`🚀 Server ready at localhost:${PORT}`);
});
