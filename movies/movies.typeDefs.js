import { gql } from 'apollo-server';

// The GraphQL schema

export default gql`
  type Movie {
    "A simple type for getting started!"
    id: Int!
    title: String!
    year: Int!
    genre: String
    createdAt: String!
    updatedAt: String!
  }
  type Query {
      movies: [Movie]
      movie(id: Int!): Movie
  }
  type Mutation {
      createMovie(title: String!, year: Int!, genre: String): Movie
      deleteMovie(id: Int!): Movie
      updateMovie(id: Int, year: Int!): Movie
  }
`;
